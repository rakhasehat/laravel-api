<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'buku';

    protected $fillable = ['kode_buku', 'judul', 'pengarang', 'tahun_terbit'];

    public function pinjaman()
    {
        return $this->hasMany(Pinjaman::class);
    }
}
