<?php

namespace App\Http\Controllers;

use App\Pinjaman;
use Illuminate\Http\Request;

class PinjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pinjaman::all();
        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $pinjaman = Pinjaman::create($this->pinjamStore());
        return response()->json([
                'message' => 'Berhasil tambah data',
                'data' => $pinjaman
        ], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tgl_kembali' => 'required',
            'status' => 'required',
        ]);

        $result = Pinjaman::where('id',$id)->update([
                        'tgl_kembali' => $request->tgl_kembali,
                        'status' => $request->status,
        ]);

        $hasil = Pinjaman::find($id);
        return response()->json([
            'data' => $hasil
        ]);;
    }

    public function pinjamStore()
    {
        return [
            'mahasiswa_id' => request('mahasiswa_id'),
            'buku_id' => request('buku_id'),
            'tgl_pinjam' => request('tgl_pinjam'),
            'tgl_batas' => request('tgl_batas'),
            'tgl_kembali' => request('tgl_kembali'),
            'status' => request('status')
        ];
    }
}
