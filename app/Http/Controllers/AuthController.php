<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class AuthController extends Controller
{
    public function login(Request $request){
        $credentials = $request->only(['username', 'password']);

        config()->set( 'auth.defaults.guard', 'api' );
        if (!$token = JWTAuth::attempt($credentials)) {
           return 'Invalid login details';
        }

        return response()->json(compact('token'), 200);
     }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create([
            'name' => $request->get('name'),
            'username' => $request->get('username'),
            'password' => Hash::make($request->get('password')),
            'role' => $request->get('role')
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user'), 201);
    }

    public function logout(Request $request)
    {
        #GET JWT Token
        $token = $request->header('Authorization');

        try{
            JWTAuth::invalidate($token);
            return response()->json([
                'status' => 'success',
                'message' => 'Logout sukses',
            ], 204);
        } catch (JWTException $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e
            ], 500);
        }
    }

    public function getUserInfo(){
        try{
            if(! $user = JWTAuth::parseToken()->authenticate()){
                return response()->json(['user_not_found'], 404);
            }
        }
        catch (TokenExpiredException $e){
            return response()->json(['token_expired'], $e->getStatusCode());
        }
        catch (TokenInvalidException $e){
            return response()->json(['token_invalid'], $e->getStatusCode());
        }

        return response()->json(compact('user'));
    }
}
