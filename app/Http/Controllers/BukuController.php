<?php

namespace App\Http\Controllers;

use App\Buku;
use Illuminate\Http\Request;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllBuku()
    {
        $data = Buku::all();
        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }
}
