<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pinjaman extends Model
{
    protected $table = 'pinjaman';

    protected $fillable = ['mahasiswa_id', 'buku_id', 'tgl_pinjam', 'tgl_batas',
                            'tgl_kembali', 'status'];

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class);
    }

    public function buku()
    {
        return $this->belongsTo(Buku::class);
    }
}
