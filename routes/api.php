<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');

Route::middleware('role.auth:admin')->group(function() {
    Route::get('info', 'AuthController@getUserInfo');
    Route::get('logout', 'AuthController@logout');

    // Route::resource('pinjam', 'PinjamanController');
    Route::get('pinjam', 'PinjamanController@index');
    Route::post('pinjam', 'PinjamanController@store');
    Route::patch('pinjam/{id}', 'PinjamanController@update');
    Route::resource('mahasiswa', 'MahasiswaController');
});


